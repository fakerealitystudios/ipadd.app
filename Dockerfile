FROM python:3

LABEL maintainer="mtuska@bluestreamfiber.com"

COPY . /src
WORKDIR /src
RUN pip install -r requirements.txt
ENTRYPOINT ["python", "./main.py"]
EXPOSE 8080/tcp