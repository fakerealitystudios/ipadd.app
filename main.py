#!/usr/bin/env python3
from http.server import HTTPServer, SimpleHTTPRequestHandler
from socketserver import ThreadingMixIn
import ipinfo
import json
import os
handler = ipinfo.getHandler(os.environ.get('IPINFO_TOKEN'))

class Handler(SimpleHTTPRequestHandler):
    def do_GET(self):
        details = None
        if self.path == "/":
            details = handler.getDetails(self.headers.get('CF-Connecting-IP', self.headers.get('DO-Connecting-IP', self.headers.get('X-Real-IP', self.client_address[0]))))

        if details is not None:
            data = {}
            data['location'] = {}
            try:
                org = details.org.split(' ')
                data['asn'] = org.pop(0)
                data['organization'] = ' '.join(org)
            except AttributeError:
                data['asn'] = None
                data['organization'] = None
            try:
                data['ip_address'] = details.ip
            except AttributeError:
                data['ip_address'] = self.headers.get('X-Real-IP', self.client_address[0])
            try:
                data['hostname'] = details.hostname
            except AttributeError:
                data['hostname'] = None
            try:
                data['timezone'] = details.timezone
            except AttributeError:
                data['timezone'] = None
            try:
                data['location']['latitude'] = details.latitude
            except AttributeError:
                data['location']['latitude'] = None
            try:
                data['location']['longitude'] = details.longitude
            except AttributeError:
                data['location']['longitude'] = None
            try:
                data['location']['country_code'] = details.country
            except AttributeError:
                data['location']['country_code'] = None
            try:
                data['location']['country'] = details.country_name
            except AttributeError:
                data['location']['country_code'] = None
            try:
                data['location']['region'] = details.region
            except AttributeError:
                data['location']['region'] = None
            try:
                data['location']['city'] = details.city
            except AttributeError:
                data['location']['city'] = None
            try:
                data['location']['postal'] = details.postal
            except AttributeError:
                data['location']['postal'] = None

            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps(data, indent=4, sort_keys=True).encode('utf-8'))
        else:
            return super().do_GET()
class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass

if __name__ == '__main__':
    server = ThreadingSimpleServer(('0.0.0.0', 8080), Handler)
    server.serve_forever()